package http

import (
	"encoding/json"
	"io/ioutil"
	"lesson-9-homework/src/config"
	"lesson-9-homework/src/domain"
	"net/http"
)

type userQueryRepo struct {
	usersUrl string
}

func NewUserQueryRepo() domain.UserQueryRepo {
	baseUrl := config.AllConfigs.RPC.ConnectionUrl
	return &userQueryRepo{usersUrl: baseUrl + "/users"}
}

func (userQueryRepo *userQueryRepo) GetUsersFromAPI() (*[]domain.User, error) {
	var users []domain.User
	response, err := http.Get(userQueryRepo.usersUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &users)
	if err != nil {
		return nil, err
	}
	return &users, err
}