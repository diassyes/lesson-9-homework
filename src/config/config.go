package config

import (
	"encoding/json"
	"fmt"
	"os"
)

// Config structures & vars ------------------
var AllConfigs *Configs

type Configs struct {
	ScyllaDB ScyllaDBConfig `json:"scylla_db"`
	RPC RpcConfig `json:"rpc"`
}

type RpcConfig struct {
	ConnectionUrl string `json:"connection_url"`
}

type ScyllaDBConfig struct {
	ConnectionIp []string `json:"connection_ip"`
	KeySpace     string   `json:"key_space"`
}
// -------------------------------------------

// Get configs -------------------------------
func GetConfigs() error {

	var filePath string

	if os.Getenv("config") != "" {
		filePath = os.Getenv("config")
	} else {
		currentDir, err := os.Getwd()
		if err != nil {
			fmt.Println("get current dir err: ", err)
			os.Exit(1)
		}

		filePath = currentDir + "/src/config/config.json"
	}

	file, err := os.Open(filePath)
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&AllConfigs)
	if err != nil {
		return err
	}

	return nil
}
// -------------------------------------------