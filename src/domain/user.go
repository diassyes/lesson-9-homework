package domain

import (
	"github.com/google/uuid"
)

type User struct {
	Uid      string  `json:"uid,omitempty"`
	Id       int     `json:"id,omitempty"`
	Name     string  `json:"name,omitempty"`
	Username string  `json:"username,omitempty"`
	Email    string  `json:"email,omitempty"`
	Address  Address `json:"address,omitempty"`
	Phone    string  `json:"phone,omitempty"`
	Website  string  `json:"website,omitempty"`
	Company  Company `json:"company,omitempty"`
}

type Address struct {
	Street  string `json:"street,omitempty"`
	Suite   string `json:"suite,omitempty"`
	City    string `json:"city,omitempty"`
	ZipCode string `json:"zipCode,omitempty"`
	Geo     Geo    `json:"geo,omitempty"`
}

type Geo struct {
	Lat string `json:"lat,omitempty"`
	Lng string `json:"lng,omitempty"`
}

type Company struct {
	Name        string `json:"name,omitempty"`
	CatchPhrase string `json:"catchPhrase,omitempty"`
	Bs          string `json:"bs,omitempty"`
}

func (user *User) GenerateUid() {
	user.Uid = uuid.New().String()
}

// User Interfaces ------------------

type UserQueryRepo interface {
	GetUsersFromAPI() (*[]User, error)
}

type UserCommandRepo interface {
	StoreUsers(*[]User) error
	GetUsersFromDB() (*[]User, error)
	// DeleteById(string) error
}
