package scylladb

import (
	"encoding/json"
	"github.com/gocql/gocql"
	"lesson-9-homework/src/domain"
)

type userCommandRepo struct {
	dbSession *gocql.Session
}

func NewUserCommandRepo(session *gocql.Session) domain.UserCommandRepo {
	return &userCommandRepo{dbSession: session}
}

// Insert all users to ScyllaDB
func (userCR *userCommandRepo) StoreUsers(users *[]domain.User) error {

	batch := userCR.dbSession.NewBatch(gocql.LoggedBatch)

	for i := 0; i < len(*users); i++ {
		userAddress, err := json.Marshal((*users)[i].Address)
		if err != nil {
			return err
		}
		userCompany, err := json.Marshal((*users)[i].Company)
		if err != nil {
			return err
		}

		batch.Query(
			"INSERT INTO user (id, user_id, name, username, email, address, phone, website, company) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
			(*users)[i].Uid,
			(*users)[i].Id,
			(*users)[i].Name,
			(*users)[i].Username,
			(*users)[i].Email,
			string(userAddress),
			(*users)[i].Phone,
			(*users)[i].Website,
			string(userCompany),
		)
	}
	err := userCR.dbSession.ExecuteBatch(batch)
	if err != nil {
		return err
	}
	return nil
}

// Get all users from ScyllaDB
func (userCR *userCommandRepo) GetUsersFromDB() (*[]domain.User, error) {

	var (
		users       []domain.User
		userAddress domain.Address
		userCompany domain.Company
	)

	mapper := map[string]interface{}{}
	iter := userCR.dbSession.Query("SELECT * FROM user").Iter()

	for iter.MapScan(mapper) {
		err := json.Unmarshal([]byte(mapper["address"].(string)), &userAddress)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal([]byte(mapper["company"].(string)), &userCompany)
		if err != nil {
			return nil, err
		}

		users = append(
			users,
			domain.User{
				Uid:      mapper["id"].(gocql.UUID).String(),
				Id:       mapper["user_id"].(int),
				Name:     mapper["name"].(string),
				Username: mapper["username"].(string),
				Email:    mapper["email"].(string),
				Address:  userAddress,
				Phone:    mapper["phone"].(string),
				Website:  mapper["website"].(string),
				Company:  userCompany,
			},
		)
		mapper = map[string]interface{}{}
	}

	return &users, nil
}
