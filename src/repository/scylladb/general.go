package scylladb

import (
	"github.com/gocql/gocql"
	"lesson-9-homework/src/config"
)

func DBConnectionStart() (*gocql.Session, error) {
	cluster := gocql.NewCluster(config.AllConfigs.ScyllaDB.ConnectionIp...)
	cluster.Consistency = gocql.Quorum
	cluster.Keyspace = config.AllConfigs.ScyllaDB.KeySpace

	client, err := cluster.CreateSession()
	if err != nil {
		return nil, err
	}

	return client, nil
}