package user

import (
	"lesson-9-homework/src/domain"
)

type Service interface {
	GetUsers() (*[]domain.User, bool, error)
	SaveUsers(*[]domain.User) error
}

type service struct {
	userQueryRepo   domain.UserQueryRepo
	userCommandRepo domain.UserCommandRepo
}

func NewService(userQueryRepo domain.UserQueryRepo, userCommandRepo domain.UserCommandRepo) Service {
	return &service{
		userQueryRepo:   userQueryRepo,
		userCommandRepo: userCommandRepo,
	}
}

func (service *service) GetUsers() (*[]domain.User, bool, error) {
	// if the users in the DB ----------------------------
	users, err := service.userCommandRepo.GetUsersFromDB()
	if err != nil {
		return nil, false, err
	}
	if *users != nil {
		return users, true, nil
	}

	// if users are not in the DB ------------------------
	users, err = service.userQueryRepo.GetUsersFromAPI()
	if err != nil {
		return nil, false, err
	}
	for i := 0; i < len(*users); i++ {
		(*users)[i].GenerateUid()
	}
	return users, false, nil
}

func (service *service) SaveUsers(users *[]domain.User) error {
	err := service.userCommandRepo.StoreUsers(users)
	if err != nil {
		return err
	}
	return nil
}
