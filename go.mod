module lesson-9-homework

go 1.13

require (
	github.com/gocql/gocql v0.0.0-20200228163523-cd4b606dd2fb
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/uuid v1.1.1
)
