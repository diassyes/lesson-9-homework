package main

import (
	"lesson-9-homework/src/config"
	"lesson-9-homework/src/repository/scylladb"
	rpcHTTP "lesson-9-homework/src/rpc/http"
	"lesson-9-homework/src/user"
)

func main() {
	err := config.GetConfigs()
	if err != nil {
		panic(err)
	}

	scyllaDBConnection, err := scylladb.DBConnectionStart()
	if err != nil {
		panic(err)
	}
	defer scyllaDBConnection.Close()

	userCommandRepo := scylladb.NewUserCommandRepo(scyllaDBConnection)
	userQueryRepo := rpcHTTP.NewUserQueryRepo()

	userService := user.NewService(
		userQueryRepo,
		userCommandRepo,
	)

	// At first tries to pull the data out of the database, but if it's not there, pulls it out of the API
	usersData, isInDatabase, err := userService.GetUsers()
	if err != nil {
		panic(err)
	}

	// If the users is not in the database, it saves the values in the database, in another case it skips
	if !isInDatabase {
		err = userService.SaveUsers(usersData)
		if err != nil {
			panic(err)
		}
	}
}
